package android.main.java.com.plugin.record_mp3.record;

/**
 * Created by hujie on 16/5/24.
 */
public enum RecordStatus {

    IDEL,
    START,
    RESUME,
    PAUSE,
    BREAK,
    STOP,
    COMPLETE
}
