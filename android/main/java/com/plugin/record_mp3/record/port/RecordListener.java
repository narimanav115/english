package android.main.java.com.plugin.record_mp3.record.port;


import com.plugin.record_mp3.record.RecordStatus;

/**
 * Created by hujie on 16/5/24.
 */
public interface RecordListener {

    /**
     * @param status
     */
    void onRecorderStatusChange(RecordStatus status);

    void onFileNotFound();

    void onPermissionError();

    void onComplete();

    void onIOExecption();

    void onRecordMayUsed();
}
