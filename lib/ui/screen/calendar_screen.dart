import 'package:aseforenglish/data/model_for_view/topic_for_view.dart';
import 'package:aseforenglish/data/models/process_learning.dart';
import 'package:aseforenglish/data/models/topic.dart';
import 'package:aseforenglish/data/repo/repository.dart';
import 'package:aseforenglish/ui/screen/process_learning_screen.dart';
import 'package:aseforenglish/ui/screen/setting_Mexanica.dart';
import 'package:aseforenglish/ui/widgets/app_bar.dart';
import 'package:aseforenglish/ui/widgets/popups.dart';
import 'package:aseforenglish/utils/constants.dart';
import 'package:aseforenglish/utils/enums.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Экран отображения Календаря повторений.
///
/// Записи в данный экран попазают после прохождения первого шага процесса
/// обучения LEARNING, в дальнейшим пользователь должен работать с данным экраном.
///

class CalendarScreen extends StatefulWidget {
  static final String id = 'calendar_screen';

  const CalendarScreen({Key key}) : super(key: key);

  @override
  _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  SharedPreferences _prefs;
  final String _title = 'Календарь повторений';

  final String _textLineFirst = 'Повторить сегодня';
  final String _textLineSecond = 'Запланированные повторения';
  bool _flutter = false;

  // В данный лист записываются сами процессы повторения, так же другие
  // элементы интерфеса, если таки существуют.
  List<Widget> list = [];

  // Базовые переменные, которые отображаются на экране
  String courseTitle = '';
  String courseGrade = '';

  bool boolM1Value;
  bool boolM2Value;
  bool boolM3Value;
  bool boolM4Value;
  bool boolM5Value;

  _callAlertDelayedSwitch(BuildContext context, TopicForView topic) async {
    showDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Center(
          child: Text(
            "Выберите типы упражнений\nдля повторения материала\n\nПри желании, Вы можете\nизменить свой выбор\nв любой момент",
            style: kSFProR_Black_14,
            textAlign: TextAlign.center,
          ),
        ),

        //

        content: NewWidget(),
        actions: [
          CupertinoDialogAction(
            child: Text(
              "Готово",
              style: kSFProR_Blue_18,
            ),
            onPressed: () {
              if ((SettingMexanica.boolM1Value == false) &&
                  (SettingMexanica.boolM2Value == false) &&
                  (SettingMexanica.boolM3Value == false) &&
                  (SettingMexanica.boolM4Value == false) &&
                  (SettingMexanica.boolM5Value == false)) {
                final popup = popupCupertinoDialogActionOneButtons(
                  title: "Подсказка",
                  subtitle: "Необходимо выбрать хотя бы одно упражнение",
                  textButton: "Ок",
                  method: () async {
                    Navigator.pop(context);
                  },
                );

                popup.showCupertinoDialogAction(context);
              } else {
                saveMexanica();
                initSettingMexanica();

                SettingMexanica.boolM1Value = boolM1Value;
                SettingMexanica.boolM2Value = boolM2Value;
                SettingMexanica.boolM3Value = boolM3Value;
                SettingMexanica.boolM4Value = boolM4Value;
                SettingMexanica.boolM5Value = boolM5Value;

                var screenArguments = ScreenArgumentsCalendarScreen(
                    topic: topic, isReiteration: true);
                Navigator.pushNamed(context, ProcessLearningScreen.id,
                    arguments: screenArguments);
              }
            },
          ),
        ],
      ),
    );
  }

  initSettingMexanica() async {
    boolM1Value = await SettingMexanica.loadDataM1();
    boolM2Value = await SettingMexanica.loadDataM2();
    boolM3Value = await SettingMexanica.loadDataM3();
    boolM4Value = await SettingMexanica.loadDataM4();
    boolM5Value = await SettingMexanica.loadDataM5();
  }

  saveMexanica() {
    boolM1Value = SettingMexanica.boolM1Value;
    boolM2Value = SettingMexanica.boolM2Value;
    boolM3Value = SettingMexanica.boolM3Value;
    boolM4Value = SettingMexanica.boolM4Value;
    boolM5Value = SettingMexanica.boolM5Value;

    SettingMexanica.saveData(
        boolM1Value, boolM2Value, boolM3Value, boolM4Value, boolM5Value);
  }

  @override
  void initState() {
    super.initState();
    initSettingMexanica();
    if (boolM1Value == null ||
        boolM2Value == null ||
        boolM3Value == null ||
        boolM4Value == null ||
        boolM5Value == null)
      SettingMexanica.saveData(
          SettingMexanica.boolM1Value,
          SettingMexanica.boolM2Value,
          SettingMexanica.boolM3Value,
          SettingMexanica.boolM4Value,
          SettingMexanica.boolM5Value);

    setList();
  }

  /*
  //срабатывает после прорисовки интерфейса
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Future.delayed(Duration(milliseconds: 1000), () {
      _callAlertDelayedSwitch(context);
    });
  }

   */

  // Метод "сборки" нашего листа
  void setList() async {
    List<Widget> result = [];
    var lastCourse = await RepositoryLocal().getLastCourse();
    // Получаем все процессы прошедшей и текущей даты
    var prevProcessList =
        await RepositoryLocal().getPrevRepeats(lastCourse.uid);
    // Получаем процесс которые нужно будет повторять в будущем
    var nextProcessList =
        await RepositoryLocal().getNextRepeats(lastCourse.uid);

    if (prevProcessList.isNotEmpty) {
      // Добавляем плаху текущих повторений
      result.add(titleRow(_textLineFirst));
      // преобразовываем лист процессов в лист тем для представления
      List<TopicForView> topicsForView =
          await getTopicForViewList(prevProcessList);
      // Результат формируем в виджеты и добавляем в лист
      result.addAll(
        topicsForView.map(
          (topic) => CalendarItemList(
            onPress: () {
              Future.delayed(Duration(milliseconds: 1000), () {
                _callAlertDelayedSwitch(context, topic);
              });
              // Navigator.pushNamed(context, ProcessLearningScreen.id,
              //     arguments: topic);
            },
            title: topic.title,
            date: '${topic.process.startDT}',
            wordSize: '${topic.countWords}',
            processLevel: topic.process.processLevel,
          ),
        ),
      );
    }

    if (nextProcessList.isNotEmpty) {
      // Добавляем плаху будущих повторений
      result.add(titleRow(_textLineSecond));
      // преобразовываем лист процессов в лист тем для представления
      List<TopicForView> topicsForView =
          await getTopicForViewList(nextProcessList);
      // Результат формируем в виджеты и добавляем в лист
      result.addAll(
        topicsForView.map(
          (topic) => CalendarItemList(
            onPress: null,
            title: topic.title,
            date: '${topic.process.startDT}',
            wordSize: '${topic.countWords}',
            processLevel: topic.process.processLevel,
          ),
        ),
      );
    }

    setState(() {
      courseTitle = lastCourse.name;
      courseGrade = lastCourse.grade;
      list = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Material(
          color: kGreyBackground,
          child: CustomScrollView(
            slivers: [
              // Отображение Названия курса в AppBar
              SliverPersistentHeader(
                delegate: AseSliverAppBar(
                  title: _title,
                  courseTitle: courseTitle,
                  courseRang: courseGrade,
                ),
                pinned: true,
              ),
              SliverList(
                  delegate: SliverChildBuilderDelegate(
                (context, int index) {
                  var element = list[index];
                  return element;
                },
                childCount: list.length,
              )),
            ],
          ),
        ),
      ),
    );
  }
}

class ScreenArgumentsCalendarScreen {
  bool isReiteration;
  TopicForView topic;

  ScreenArgumentsCalendarScreen({this.isReiteration, this.topic});
}

class NewWidget extends StatefulWidget {
  NewWidget({
    Key key,
    bool value1,
    bool value2,
    bool value3,
    bool value4,
    bool value5,
  }) : super(key: key);

  @override
  _NewWidgetState createState() => _NewWidgetState();
}

class _NewWidgetState extends State<NewWidget> {
  void _onChanged1(bool value) =>
      setState(() => SettingMexanica.boolM1Value = value);

  void _onChanged2(bool value) =>
      setState(() => SettingMexanica.boolM2Value = value);

  void _onChanged3(bool value) =>
      setState(() => SettingMexanica.boolM3Value = value);

  void _onChanged4(bool value) =>
      setState(() => SettingMexanica.boolM4Value = value);

  void _onChanged5(bool value) =>
      setState(() => SettingMexanica.boolM5Value = value);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 25,
            width: 25,
          ),
          Center(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Выбор перевода",
                      style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    CupertinoSwitch(
                        activeColor: Colors.green,
                        value: SettingMexanica.boolM1Value,
                        onChanged: (bool value) {
                          _onChanged1(value);
                        }),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Выбор пропущенного слова",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                        )),
                    CupertinoSwitch(
                        activeColor: Colors.green,
                        value: SettingMexanica.boolM2Value,
                        onChanged: (bool value) {
                          _onChanged2(value);
                        }),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Написать пропущенное слово",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                        )),
                    CupertinoSwitch(
                        activeColor: Colors.green,
                        value: SettingMexanica.boolM3Value,
                        onChanged: (bool value) {
                          _onChanged3(value);
                        }),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Верно/Неверно",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                        )),
                    CupertinoSwitch(
                        activeColor: Colors.green,
                        value: SettingMexanica.boolM4Value,
                        onChanged: (bool value) {
                          _onChanged4(value);
                        }),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Тренировка произношения",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                        )),
                    CupertinoSwitch(
                        activeColor: Colors.green,
                        value: SettingMexanica.boolM5Value,
                        onChanged: (bool value) {
                          _onChanged5(value);
                        }),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 18.0,
          ),
        ],
      ),
    );
  }

//метод получения значения
// void _loadBoolPref(String BoolMX) {
//   switch (BoolMX) {
//     case BoolM1key:
//       setState(() {
//         boolM1value = _prefs.getBool(BoolM1key);
//       });
//       break;
//     case BoolM2key:
//       setState(() {
//         boolM2value = _prefs.getBool(BoolM2key);
//       });
//       break;
//     case BoolM1key:
//       setState(() {
//         boolM3value = _prefs.getBool(BoolM1key);
//       });
//       break;
//     case BoolM1key:
//       setState(() {
//         boolM1value = _prefs.getBool(BoolM1key);
//       });
//       break;
//     case BoolM1key:
//       setState(() {
//         boolM1value = _prefs.getBool(BoolM1key);
//       });
//       break;
//   }
// }

}

/// Асинхронный метод способный переформировать лист Процессов в лист Тем для представления.
///
/// В основном нужно для получения информации которой нет в процессах
/// а именно названия Темы и количества слов в данной теме
///
/// Данная информация отображается в листе
///
/// [list] - соответственно лист процессов.
Future<List<TopicForView>> getTopicForViewList(
    List<ProcessLearning> list) async {
  List<TopicForView> topicsForView = [];
  for (int i = 0; i < list.length; i++) {
    ProcessLearning proc = list[i];
    Topic topic = await RepositoryLocal().getTopic(proc.topicID);
    int wordSize = await RepositoryLocal().getCountCardsInTopic(proc.topicID);
    topicsForView.add(
      TopicForView(
        uid: topic.uid,
        title: topic.name,
        type: TopicType.TOPIC,
        parentID: topic.upID,
        process: proc,
        countWords: wordSize,
      ),
    );
  }
  return topicsForView;
}

/// Виджет для отображения плашек в листе
/// "Запланированые повторения" и "Повторить сегодня"
///
/// [title] - текстовое поле
Widget titleRow(String title) {
  return Container(
    width: double.infinity,
    color: kGreyBackground,
    padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
    child: Text(
      title,
      textAlign: TextAlign.start,
      style: kSFProR_Grey_14,
    ),
  );
}

/// Виджет отображения элеменотв списка повторений
///
/// [title] - Название темы
///
/// [date] - Дата повторения
///
/// [wordSize] - Количество слов в теме
///
/// [processLevel] - позиция в процессе повторения
///
/// [onPress] - Функция отработки нажатия
///
class CalendarItemList extends StatelessWidget {
  final String title;
  final String date;
  final String wordSize;
  final ProcessLevel processLevel;

  final Function onPress;

  CalendarItemList({
    @required this.title,
    @required this.date,
    @required this.wordSize,
    @required this.processLevel,
    @required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.all(0.0),
      onPressed: onPress,
      child: Container(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 10.0,
          ),
          decoration: BoxDecoration(
              color: kAppBarColor,
              border: Border.all(
                color: kGreyBackground,
                width: 0.5,
              )),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '$title',
                      style: kSFProR_Black_18,
                    ),
                    Row(
                      children: <Widget>[
                        //TODO тут в если задание просрочено, то подсвечиваем красным.
                        Text(
                          date.substring(0, 10),
                          style: kSFProR_Black_14,
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 16.0),
                          padding: EdgeInsets.symmetric(
                            horizontal: 6.0,
                            vertical: 0.5,
                          ),
                          decoration: BoxDecoration(
                            color: kWordSizeBasckground,
                            borderRadius: BorderRadius.all(
                              Radius.circular(9.0),
                            ),
                          ),
                          child: Text(
                            wordSize,
                            style: kSFProR_White_16,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Text(
                '${getProcessLevelToInt(processLevel)} из 7',
                style: kSFProR_Grey_14,
              ),
              Icon(
                Icons.chevron_right,
                size: 24.0,
                color: kWordSizeBasckground,
              )
            ],
          ),
        ),
      ),
    );
  }
}

// _callAlertDelayed(BuildContext context) async {
//   final popup = showCupertinoDialogActionSwitch(
//     title: "Подсказка",
//     subtitle:
//     "Послушайте как диктор\nпроизносит слово (фразу).\n\nЗатем произнесите слово (фразу)\nсамостоятельно и запишите свой голос.\n\nПо необходимости повторите последовательность действий.",
//     textButton: "Ок",
//     method: () async {
//       Navigator.pop(context);
//     },
//   );
// }

// child: new Center(
// child: new Column(
// children: <Widget>[
// new Switch(value: _value1, onChanged: _onChanged1),
// new SwitchListTile(
// value: _value2,
// onChanged: _onChanged2,
// title: new Text('Hello World', style: new TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
// )
// ],
//

//отображаю группу switchей

// class widgetSwitchGroup extends StatefulWidget {
//   @override
//   _State createState() => _State();
// }
//
// //State is information of the application that can change over time or when some actions are taken.
// class _State extends State<widgetSwitchGroup> {
//   bool _value1 = false,
//       _value2 = false,
//       _value3 = false,
//       _value4 = false,
//       _value5 = false;
//
//   void _onChanged1(bool value) => setState(() => _value1 = value);
//   void _onChanged2(bool value) => setState(() => _value2 = value);
//   void _onChanged3(bool value) => setState(() => _value3 = value);
//   void _onChanged4(bool value) => setState(() => _value4 = value);
//   void _onChanged5(bool value) => setState(() => _value5 = value);
//
//   //виджет отображения Switch
//
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Column(
//         children: <Widget>[
//           SwitchListTile(
//             value: false,
//             onChanged: _onChanged1,
//             title: Text('Выбор перевода',
//                 style:
//                     TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
//           ),
//           SwitchListTile(
//             value: _value2,
//             onChanged: _onChanged2,
//             title: Text('Выбор пропущенного слова',
//                 style:
//                     TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
//           ),
//           SwitchListTile(
//             value: _value3,
//             onChanged: _onChanged3,
//             title: Text('Написать пропущенное слово',
//                 style:
//                     TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
//           ),
//           SwitchListTile(
//             value: _value4,
//             onChanged: _onChanged4,
//             title: Text('Верно/Неверно',
//                 style:
//                     TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
//           ),
//           SwitchListTile(
//             value: _value5,
//             onChanged: _onChanged5,
//             title: Text('Тренировка произношения',
//                 style:
//                     TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
//           ),
//         ],
//       ),
//     );
//   }
// }

//в диалоговое окно вставляю свой класс с Switch
