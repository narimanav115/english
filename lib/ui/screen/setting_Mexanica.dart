import 'package:shared_preferences/shared_preferences.dart';

//нужен для выбора механики для окна со swich
class SettingMexanica {

  static bool boolM1Value = true;
  static bool boolM2Value = true;
  static bool boolM3Value = true;
  static bool boolM4Value = false;
  static bool boolM5Value = false;

  static saveData(bool boolM1Value, bool boolM2Value, bool boolM3Value,
      bool boolM4Value, bool boolM5Value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool("BoolM1key", boolM1Value);
    await preferences.setBool("BoolM2key", boolM2Value);
    await preferences.setBool("BoolM3key", boolM3Value);
    await preferences.setBool("BoolM4key", boolM4Value);
    await preferences.setBool("BoolM5key", boolM5Value);
    return true;
  }

  static Future<bool> loadDataM1() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool("BoolM1key");
  }

  static Future<bool> loadDataM2() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool("BoolM2key");
  }

  static Future<bool> loadDataM3() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool("BoolM3key");
  }

  static Future<bool> loadDataM4() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool("BoolM4key");
  }

  static Future<bool> loadDataM5() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool("BoolM5key");
  }

  static SetDataM1() {
    loadDataM1().then((value) {
      SettingMexanica.boolM1Value = value;
    });
  }

  static SetDataM2() {
    loadDataM2().then((value) {
      SettingMexanica.boolM2Value = value;
    });
  }

  static SetDataM3() {
    loadDataM3().then((value) {
      SettingMexanica.boolM3Value = value;
    });
  }

  static SetDataM4() {
    loadDataM4().then((value) {
      SettingMexanica.boolM4Value = value;
    });
  }

  static SetDataM5() {
    loadDataM5().then((value) {
      SettingMexanica.boolM5Value = value;
    });
  }
}
