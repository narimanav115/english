import 'package:aseforenglish/ui/widgets/popups.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:record_mp3/record_mp3.dart';
import 'package:aseforenglish/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';
import 'package:audioplayers/audioplayers.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:shared_preferences/shared_preferences.dart';

/// Виджет отображения 5 механики.
final keyIsFirstLoaded = 'is_first_loaded';
AudioPlayerState audioPlayerState;

// ignore: must_be_immutable
class MFive extends StatefulWidget {
  MFive(
      {Key key,
      @required this.wordText,
      @required this.audioPath,
      @required this.playAudio,
      @required this.isAudioPlaying,
      @required this.userAnswer,
      this.isBoolMFiveOld})
      : super(key: key);
  final String wordText;
  final String audioPath;
  final Function playAudio;
  final bool isAudioPlaying;
  final Function userAnswer;
  bool isBoolMFiveOld;

  @override
  _MFiveState createState() => _MFiveState();
}

AudioCache firstCacheAudio;
AudioCache audioCacheUp;
AudioCache audioCacheRecording;
AudioCache audioCachePip;

AudioPlayer firstPlayAudio;
AudioPlayer audioPlayUp;
AudioPlayer audioPlayerPip;
AudioPlayer audioPlayerRecording;

Duration _durationDiktor = Duration();
Duration _positionDiktor = Duration();
Duration _sliderDiktor = Duration(seconds: 0);
Duration _durationRecording = Duration();
Duration _positionRecording = Duration();
Duration _sliderRecording = Duration(seconds: 0);

//задержка в секундах
int delay = 5;

class _MFiveState extends State<MFive> with TickerProviderStateMixin {
  bool _isPlayAudioWord = false;
  var inValueDoubleSliderReplay = 0.0;
  var inValueDoubleSliderRecords = 0.0;
  bool isPressed = true;
  String audioPip;
  bool isDisabledPlayer = false;
  bool isDisabledRecording = false;
  bool nagatie = false;

  void seekToSecondDiktor(int second) {
    Duration newDuration = Duration(seconds: second);
    firstPlayAudio.seek(newDuration);
  }

  void seekToSecondRecording(int second) {
    Duration newDuration = Duration(seconds: second);
    audioPlayerRecording.seek(newDuration);
  }

  @override
  initState() {
    super.initState();

    if (Platform.isAndroid)
      audioPip = "pip.mp3";
    else if (Platform.isIOS) audioPip = "/pip.mp3";

    isLeftButtonActive = widget.isBoolMFiveOld;
    isRightButtonActive = widget.isBoolMFiveOld;
    _callAlertDelayed(context);
    audioPathNew = widget.audioPath.replaceAll('assets/', '');
    _initAudioPlayer();
    _positionRecording = _sliderRecording;
    _positionDiktor = _sliderDiktor;
    _listenerPlay();
  }

  _initAudioPlayer() {
    audioPlayerPip = AudioPlayer();
    audioCachePip = AudioCache(fixedPlayer: audioPlayerPip);

    firstPlayAudio = AudioPlayer();
    firstCacheAudio = AudioCache(fixedPlayer: firstPlayAudio);

    audioPlayUp = AudioPlayer();
    audioCacheUp = AudioCache(fixedPlayer: audioPlayUp);

    audioPlayerRecording = AudioPlayer();
    audioCacheRecording = AudioCache(fixedPlayer: audioPlayerRecording);
  }

  @override
  dispose() {
    super.dispose();
    audioPlayerPip.dispose();
    firstPlayAudio.dispose();
    audioPlayUp.dispose();
    audioPlayerRecording.dispose();
  }

  String recordFilePath;
  final ScrollController controller = ScrollController();
  bool isLeftButtonActive;
  bool isRightButtonActive;
  File tmpFile;
  String audioPathNew;
  String newAudioPathNew;
  bool isMakeButtonsGray = true;

  Future<ByteData> loadAsset(path) async => await rootBundle.load(path);

  @override
  Widget build(BuildContext context) {
    if (widget.isBoolMFiveOld == false) init();
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Scrollbar(
                isAlwaysShown: false,
                controller: controller,
                child: SingleChildScrollView(
                  child: Center(
                    child: Text(
                      widget.wordText,
                      style: kSFProR_Black_18_Bold,
                      textWidthBasis: TextWidthBasis.parent,
                    ),
                  ),
                )),
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(5),
              child: Container(width: 300, child: sliderReplay()),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(5),
              child: Container(width: 300, child: sliderPlayingRecord()),
            ),
          ),
          SizedBox(
            height: 55,
            width: 55,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                        onTap: isDisabledPlayer
                            ? null
                            : () {
                                setState(() {
                                  isDisabledRecording = true;
                                });
                                isLeftButtonActive
                                    ? tappedStartPlay(
                                        newAudioPathNew, isLeftButtonActive)
                                    : tappedStartPlay(
                                        audioPathNew, isLeftButtonActive);
                              },
                        child: Container(
                          width: 80,
                          height: 80,
                          child: Image.asset(
                            isMakeButtonsGray
                                ? 'assets/images/icons/speaker_grey.png'
                                : 'assets/images/icons/speaker_blue.png',
                          ),
                        )),
                    SizedBox(
                      height: 5,
                      width: 5,
                    ),
                    isLeftButtonActive
                        ? Text("Прослушать\nи сравнить",
                            style: kSFProR_Black_16,
                            textAlign: TextAlign.center,
                            textWidthBasis: TextWidthBasis.parent)
                        : Text("Прослушать",
                            style: kSFProR_Black_16,
                            textWidthBasis: TextWidthBasis.parent),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    IgnorePointer(
                      ignoring: nagatie,
                      child: InkWell(
                          onTap: isDisabledRecording
                              ? null
                              : () {
                                  setState(() {
                                    isDisabledPlayer = true;
                                  });
                                  isPressed
                                      ? startRecord(isLeftButtonActive)
                                      : stopRecord();
                                },
                          child: Container(
                              width: 80,
                              height: 80,
                              child: rightButtonSelection())),
                    ),
                    SizedBox(
                      height: 5,
                      width: 5,
                    ),
                    isLeftButtonActive
                        ? Text("Улучшить\nрезультат",
                            style: kSFProR_Black_16,
                            textWidthBasis: TextWidthBasis.parent)
                        : Text("Записать\nсвой голос",
                            style: kSFProR_Black_16,
                            textAlign: TextAlign.center,
                            textWidthBasis: TextWidthBasis.parent),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  tappedStartPlay(String path, bool isLeftButtonActive) async {
    if ((!_isPlayAudioWord) && !isLeftButtonActive) {
      await _playAllSound(path, isLeftButtonActive);
      if (mounted)
        setState(() {
          _isPlayAudioWord = false;
        });
    } else if ((!_isPlayAudioWord) && isLeftButtonActive) {
      audioPlayUp = AudioPlayer();
      audioCacheUp = AudioCache(fixedPlayer: audioPlayUp);
      audioPlayUp.onPlayerCompletion.listen((event) async {
        await _playAllSound(path, isLeftButtonActive);
        if (mounted)
          setState(() {
            _isPlayAudioWord = false;
            inValueDoubleSliderReplay = 0.0;
            isDisabledRecording = true;
          });
      });
      audioPlayUp.onDurationChanged.listen((d) {
        if (mounted)
          setState(
            () {
              _durationDiktor = d;
              isDisabledPlayer = true;
            },
          );
      });
      audioPlayUp.onAudioPositionChanged.listen((p) {
        if (mounted)
          setState(() {
            _positionDiktor = p;
            inValueDoubleSliderReplay = _positionDiktor.inSeconds.toDouble();
          });
      });
      await audioCacheUp.play(audioPathNew);
    } else
      stopPlay();
  }

  _playAllSoundRecord(String path) async {
    audioPlayUp = AudioPlayer();
    audioCacheUp = AudioCache(fixedPlayer: audioPlayUp);
    audioPlayerPip = AudioPlayer();
    audioCachePip = AudioCache(fixedPlayer: audioPlayerPip);
    audioPlayUp.onPlayerCompletion.listen((event) {
      audioCachePip.play(audioPip);
      if (mounted)
        setState(() {
          isRightButtonActive = true;
          isPressed = false;
          nagatie = false;
        });
    });
    audioPlayerPip.onPlayerCompletion.listen((event) async {
      recordFilePath = await getFilePath();
      RecordMp3.instance.start(recordFilePath, (type) {});
      await Future.delayed(Duration(seconds: delay), () {
        stopRecord();
      });
      isDisabledPlayer = false;
    });
    setState(() {
      nagatie = true;
    });
    await audioCacheUp.play(path);
  }

  void stopRecord() {
    RecordMp3.instance.stop();
    if (mounted)
      setState(() {
        isRightButtonActive = false;
        isLeftButtonActive = true;
        isPressed = true;
        isDisabledPlayer = false;
        nagatie = false;
      });
  }

  _playAllSound(String path, bool active1) async {
    final file = await File(path);
    if (!active1) {
      firstPlayAudio.stop();
      firstPlayAudio = AudioPlayer();
      firstCacheAudio = AudioCache(fixedPlayer: firstPlayAudio);

      firstPlayAudio.onPlayerCompletion.listen((event) {
        setState(() {
          inValueDoubleSliderReplay = 0.0;
          isDisabledRecording = false;
        });
      });

      firstPlayAudio.onDurationChanged.listen((d) {
        setState(
          () {
            _durationDiktor = d;
            delay = ((_durationDiktor.inSeconds * 1.2) + 0.4).round();
          },
        );
      });
      firstPlayAudio.onAudioPositionChanged.listen((p) {
        setState(() {
          _positionDiktor = p;
          inValueDoubleSliderReplay = _positionDiktor.inSeconds.toDouble();
        });
      });

      await firstCacheAudio.play(file.path);
    } else {
      audioPlayerRecording = AudioPlayer();
      audioCacheRecording = AudioCache(fixedPlayer: audioPlayerRecording);
      audioPlayerRecording.onPlayerCompletion.listen((event) {
        if (mounted)
          setState(() {
            inValueDoubleSliderRecords = 0.0;
            isDisabledRecording = false;
            isDisabledPlayer = false;
          });
      });
      audioPlayerRecording.onDurationChanged.listen((d) {
        if (mounted)
          setState(
            () {
              isDisabledPlayer = true;
              _durationRecording = d;
            },
          );
      });

      audioPlayerRecording.onAudioPositionChanged.listen((p) {
        if (mounted)
          setState(() {
            _positionRecording = p;
            inValueDoubleSliderRecords =
                _positionRecording.inSeconds.toDouble();
          });
      });
      await audioPlayerRecording.play(path);
      widget.userAnswer();
    }
    if (mounted)
      setState(() {
        _isPlayAudioWord = false;
        active1 = true;
      });
  }

  Future<String> getFilePath() async {
    Directory directory = await getTemporaryDirectory();
    recordFilePath = '${directory.path}/new.mp3';
    tmpFile = await File(recordFilePath).create(recursive: true);
    newAudioPathNew = recordFilePath.replaceAll('assets/', '');
    return recordFilePath;
  }

  stopPlay() {
    firstPlayAudio.stop();
    audioPlayerRecording.stop();
    if (mounted)
      setState(() {
        _isPlayAudioWord = true;
      });
  }

  void _listenerPlay() {
    firstPlayAudio.onPlayerCompletion.listen((event) {
      setState(() {
        inValueDoubleSliderReplay = 0.0;
      });
    });

    firstPlayAudio.onDurationChanged.listen((d) {
      setState(
        () {
          _durationDiktor = d;
          delay = ((_durationDiktor.inSeconds * 1.2) + 0.4).round();
        },
      );
    });
    firstPlayAudio.onAudioPositionChanged.listen((p) {
      setState(() {
        _positionDiktor = p;
        inValueDoubleSliderReplay = _positionDiktor.inSeconds.toDouble();
      });
    });
  }

  void startRecord(bool isLeftButtonActive) async {
    if ((isLeftButtonActive) && (tmpFile.existsSync())) await tmpFile.delete();
    bool hasPermission = await checkPermission();
    if (hasPermission) {
      _playAllSoundRecord(audioPathNew);
    }
  }

  Future<bool> checkPermission() async {
    if (!await Permission.microphone.isGranted) {
      PermissionStatus status = await Permission.microphone.request();
      if (status != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }

  Widget sliderReplay() {
    return SliderTheme(
        data: SliderTheme.of(context).copyWith(
          activeTrackColor: Colors.yellow,
          inactiveTrackColor: Colors.blue,
          trackShape: RectangularSliderTrackShape(),
          trackHeight: 4.0,
          thumbColor: Colors.white,
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0),
          overlayColor: Colors.white.withAlpha(0),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 5.0),
        ),
        child: IgnorePointer(
          //добавил для того чтобы пользователь не мог произвольно менять индикатор прокрутки
          ignoring: true,
          child: Slider(
              value: inValueDoubleSliderReplay,
              max: _durationDiktor.inSeconds.toDouble(),
              onChanged: (double value) {
                if (mounted)
                  setState(() {
                    seekToSecondDiktor(value.toInt());
                    value = value;
                  });
              }),
        ));
  }

  Widget sliderPlayingRecord() {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        activeTrackColor: Colors.yellow,
        inactiveTrackColor: Colors.red,
        trackShape: RectangularSliderTrackShape(),
        trackHeight: 4.0,
        thumbColor: Colors.white,
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0),
        overlayColor: Colors.white.withAlpha(0),
        overlayShape: RoundSliderOverlayShape(overlayRadius: 5.0),
      ),
      child: IgnorePointer(
        //добавил для того чтобы пользователь не мог произвольно менять индикатор прокрутки
        ignoring: false,
        child: Slider(
          value: inValueDoubleSliderRecords,
          max: _durationRecording.inSeconds.toDouble(),
          onChanged: (double value) {
            if (mounted)
              setState(() {
                seekToSecondRecording(value.toInt());
                value = value;
              });
          },
        ),
      ),
    );
  }

  _callAlertDelayed(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstLoaded = prefs.getBool(keyIsFirstLoaded);
    if (isFirstLoaded == null) {
      final popup = popupCupertinoDialogActionOneButtons(
        title: "Подсказка",
        subtitle:
            "Послушайте как диктор\nпроизносит слово (фразу).\n\nЗатем произнесите слово (фразу)\nсамостоятельно и запишите свой голос.\n\nПо необходимости повторите последовательность действий.",
        textButton: "Ок",
        method: () async {
          Navigator.pop(context);
          if (mounted)
            setState(() {
              isMakeButtonsGray = false;
            });
        },
      );
      Future.delayed(Duration(milliseconds: 1500)).then((value) {
        popup.showCupertinoDialogAction(context);
        prefs.setBool(keyIsFirstLoaded, false);
      });
    } else {
      if (mounted)
        setState(() {
          isMakeButtonsGray = false;
        });
    }
  }

  init() async {
    isDisabledPlayer = false;
    isDisabledRecording = false;
    inValueDoubleSliderReplay = 0.0;
    inValueDoubleSliderRecords = 0.0;
    audioPlayerRecording.stop();
    audioPlayUp.stop();
    isLeftButtonActive = widget.isBoolMFiveOld;
    isRightButtonActive = widget.isBoolMFiveOld;
    widget.isBoolMFiveOld = true;
    Directory directory = await getTemporaryDirectory();
    String recordFilePath = '${directory.path}/new.mp3';
    File tmpFile = File(recordFilePath);
    if (tmpFile.existsSync()) tmpFile.delete();
    audioPathNew = widget.audioPath.replaceAll('assets/', '');
  }

  // ignore: missing_return
  Widget rightButtonSelection() {
    switch (isRightButtonActive) {
      case true:
        return Image.asset(
          'assets/images/icons/stop_red.png',
          width: 80,
          height: 80,
        );
      case false:
        if (isMakeButtonsGray)
          return Image.asset('assets/images/icons/recording_grey.png',
              width: 80, height: 80);
        else
          return Image.asset('assets/images/icons/recording_red.png',
              width: 80, height: 80);
    }
  }

  Future<bool> showDialogIfFirstLoaded() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstLoaded = prefs.getBool(keyIsFirstLoaded);
    if (isFirstLoaded == null)
      return true;
    else
      return false;
  }
}
