import 'dart:io';
import 'package:aseforenglish/data/databases/db_content.dart';
import 'package:aseforenglish/data/repo/repository.dart';
import 'package:aseforenglish/ui/screen/auth_screen.dart';
import 'package:aseforenglish/ui/screen/bug_screen.dart';
import 'package:aseforenglish/ui/screen/calendar_screen.dart';
import 'package:aseforenglish/ui/screen/card_screen.dart';
import 'package:aseforenglish/ui/screen/catalog_screen.dart';
import 'package:aseforenglish/ui/screen/course_home_screen.dart';
import 'package:aseforenglish/ui/screen/course_main_screen.dart';
import 'package:aseforenglish/ui/screen/find_screen.dart';
import 'package:aseforenglish/ui/screen/intro_screen.dart';
import 'package:aseforenglish/ui/screen/process_learning_screen.dart';
import 'package:aseforenglish/ui/screen/result_screen.dart';
import 'package:aseforenglish/ui/screen/setting_screen.dart';
import 'package:aseforenglish/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // Копируем данные в Базу данных.
  Future<File> get _copyDB async {
    // Для корректной работы замекны базы данных, тредуется заврыть открытые базы, если они открыты.
    await RepositoryLocal()
        .closeDB(); // Добавил закрытие Юазы сюда, для того что бы копирование не свершалось при открытой базе данных.

    // Получаем директорию приложения
    Directory directory = await getApplicationDocumentsDirectory();
    var dbPath = '${directory.path}/${DBContentProvider.BASE_NAME}';

    // Получаем побайтово заготовл brew install dartенную базу данных
    ByteData data = await rootBundle
        .load("assets/data/database/${DBContentProvider.BASE_NAME}");
    // Копируем базу данных
    List<int> bytes =
        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    return await File(dbPath).writeAsBytes(bytes);
  }

  @override
  Widget build(BuildContext context) {
    _copyDB;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: kAppTitle,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: IntroScreen.id,
      routes: {
        BugScreen.id: (_) => Scaffold(body: BugScreen()),
        AuthScreen.id: (_) => AuthScreen(),
        CardScreen.id: (_) => Scaffold(body: CardScreen()),
        FindScreen.id: (_) => Scaffold(body: FindScreen()),
        IntroScreen.id: (_) => Scaffold(body: IntroScreen()),
        ResultScreen.id: (_) => Scaffold(body: ResultScreen()),
        SettingScreen.id: (_) => Scaffold(body: SettingScreen()),
        CatalogScreen.id: (_) => Scaffold(body: CatalogScreen()),
        CalendarScreen.id: (_) => Scaffold(body: CalendarScreen()),
        CourseMainScreen.id: (_) => Scaffold(body: CourseMainScreen()), //пример
        CourseHomeScreen.id: (_) => Scaffold(body: CourseHomeScreen()),
        ProcessLearningScreen.id: (_) => Scaffold(body: ProcessLearningScreen()),
      },
    );
  }
}
